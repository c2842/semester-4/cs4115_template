add_executable(lin lin.cc)
add_executable(quad quad.cc)

# these are not currently enabled (commented out)
#add_executable(lin-silly lin-silly.cc)
#add_executable(quad-silly quad-silly.cc)